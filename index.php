<?php

require "vendor/autoload.php";


$Bar = new Picqer\Barcode\BarcodeGeneratorSVG();

$code =$Bar->getBarcode("Mohammad Sajjad", $Bar::TYPE_CODE_128);

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Barcode Generator In PHP</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <style>
        body, html {
            height: 100%;
            margin: 0 auto;
        }

        .bg{
            background-image: url("bg.jpg");
            height: 100%;
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }
        #qrbox>div{
            margin: auto;
        }
        p{
            color: white;
            margin-top: 270px;
            margin-left: 100px;
        }

    </style>
</head>
<body class="bg">
<div class="container" id="panel">
    <br><br><br>
    <div class="row">
        <div class="col-md-6 offset-md-3 mt-5" style="background: white; padding: 20px; box-shadow: 10px 10px 5px #888888">
            <div class="panel-heading">
                <h1 >Barcode Generate in PHP</h1>

            </div>
            <hr>

            <form  class="form-group" action="generate.php" method="post">
                <input type="text" name="text" class="form-control" style="border-radius: 0px;" placeholder="Type your text............." value="">

                <button class="btn btn-block btn-md btn-outline-success" type="submit">Generate </button>
            </form>
        </div>
    </div>

</div>
<div class="footer col-md-4 offset-md-8">
    <p>@2019 Developed By <a href="http://sorad.cf/resume">Sajjad</a> </p>
</div>
</body>
</html>
